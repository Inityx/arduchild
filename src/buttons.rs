//! Access to the front buttons

use bitflags::bitflags;
use atmega32u4::{
    interrupt::CriticalSection,
    PORTF, PORTE, PORTB,
};

/// Sample the front button state
///
/// Can be used with [`atmega32u4::interrupt::free`].
pub fn sample(_: &CriticalSection) -> Buttons {
    Buttons::from_raw(
        unsafe { &*PORTF::ptr() }.pin.read().bits(),
        unsafe { &*PORTE::ptr() }.pin.read().bits(),
        unsafe { &*PORTB::ptr() }.pin.read().bits(),
    )
}

bitflags! {
    /// Sampled state of the front buttons
    pub struct Buttons: u8 {
        //          URLD_AB--
        const U = 0b1000_0000;
        const R = 0b0100_0000;
        const L = 0b0010_0000;
        const D = 0b0001_0000;
        const A = 0b0000_1000;
        const B = 0b0000_0100;
        const URLD = Self::U.bits | Self::R.bits | Self::L.bits | Self::D.bits;
    }
}

impl Buttons {
    #[inline(always)]
    fn from_raw(pin_f: u8, pin_e: u8, pin_b: u8) -> Self {
        let dir = !pin_f & Self::URLD.bits;
        let a   = !pin_e & (Self::A.bits << 3);
        let b   = !pin_b & (Self::B.bits << 2);
        // If this unwrap fails, there is a bug in this method's bit logic
        Self::from_bits(dir | (a >> 3) | (b >> 2)).unwrap()
    }
}
