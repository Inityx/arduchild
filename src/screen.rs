//! Abstractions for the OLED screen

use core::mem::{transmute, size_of};

const SPDR: *mut u8 = 0x002E as *mut u8;
const SPSR: *mut u8 = 0x002D as *mut u8;

/// Screen width in pixels
pub const WIDTH: usize = 128;
/// Screen height in pixels
pub const HEIGHT: usize = 64;

/// Whether a pixel is on or off
pub enum Value { Off = 0, On = 1 }

/// Precomputed bitwise indexing for the physical screen layout
pub struct Index { byte: usize, shift: usize }

impl Index {
    /// Create an `Index` referencing a pixel at `(x, y)`
    ///
    /// Contains a debug assert that the coordinate is on the screen.
    #[inline(always)]
    pub fn at(x: usize, y: usize) -> Self {
        debug_assert!(x < WIDTH);
        debug_assert!(y < HEIGHT);
        // Evidently the screen is sideways
        // Potentially related to tetris being sideways?
        Self {
            byte: (y/8)*WIDTH + x,
            shift: (y%8),
        }
    }
}

type Buffer = [u8;(WIDTH*HEIGHT)/8];

/// In-memory screen backbuffer
///
/// The Screen's `buffer` member must be serialized to
/// SPI for its contents to be displayed.
pub struct Screen {
    storage: Buffer,
}

impl Screen {
    /// Set indexed pixel to `value`
    #[inline(always)]
    pub fn set(&mut self, index: Index, value: Value) {
        match value {
            Value::Off => self.set_off(index),
            Value::On  => self.set_on (index),
        }
    }

    /// Set indexed pixel's buffer value to [`Value::Off`]
    #[inline(always)]
    pub fn set_off(&mut self, Index { byte, shift}: Index) {
        self.storage[byte] &= !(1u8 << shift)
    }

    /// Set indexed pixel's buffer balue to [`Value::On`]
    #[inline(always)]
    pub fn set_on(&mut self, Index { byte, shift }: Index) {
        self.storage[byte] |= 1u8 << shift
    }

    /// Get the indexed pixel's buffer value
    #[inline(always)]
    pub fn get(&self, Index { byte, shift }: Index) -> Value {
        let raw_byte = self.storage[byte] | (1u8 << shift);

        unsafe { transmute(raw_byte >> shift) }
    }

    /// Write the screen buffer out to the display
    pub fn display(&self, clear: bool) {
        // From Arduboy2:
        //
        // The following assembly code runs "open loop". It relies on instruction
        // execution times to allow time for each byte of data to be clocked out.
        // It is specifically tuned for a 16MHz CPU clock and SPI clocking at 8MHz.

        // TODO fix this
        // http://llvm.org/docs/LangRef.html#inline-assembler-expressions
        // + means r/w
        // = means write-only
        // I means const 0x00-0x3F
        // M means const 0x00-0xFF
        // r means general register
        // e means pointer register
        // w means ___w compatible register
        // & means earlyclobber
        let mut cursor = self.storage.as_ptr();
        let mut count: u16;
        let [len_lsb, len_msb, ..] = (size_of::<Buffer>() * 2).to_le_bytes(); // delay loop multiplier = 2
        unsafe { asm!("
            ldi   %A1, %4                    ;        for (len = WIDTH * HEIGHT / 8)
            ldi   %B1, %5                    ;
            1:
            ld    __tmp_reg__, %a0           ;2       tmp = *(image)
            out   %2, __tmp_reg__            ;1       SPDR = tmp
            cpse  %6, __zero_reg__           ;1/2     if (clear) tmp = 0;
            mov   __tmp_reg__, __zero_reg__  ;1
            2:
            sbiw  %A1, 1                     ;2       len--
            sbrc  %A1, 0                     ;1/2     loop twice for cheap delay
            rjmp  2b                         ;2
            st    %a0+, __tmp_reg__          ;2       *(image++) = tmp
            brne  1b                         ;1/2 :18 len > 0
            in    __tmp_reg__, %3            ;        read SPSR to clear SPIF"
            : "+&e" (cursor),
              "=&w" (count)
            : "I"   (*SPDR),
              "I"   (*SPSR),
              "M"   (len_lsb),
              "M"   (len_msb),
              "r"   (clear as u8)
        ) }
    }

    /// Fill the screen buffer with either black or white
    pub fn clear(&mut self, value: Value) {
        // From Arduboy2:
        //
        // C version:
        //
        // color = color ? 0xFF : 0x00;
        // for (int16_t i = 0; i < WIDTH * HEIGTH / 8; i++)
        //    sBuffer[i] = color;
        //
        // This asm version is hard coded for 1024 bytes. It doesn't use the defined
        // WIDTH and HEIGHT values. It will have to be modified for a different
        // screen buffer size.
        // It also assumes color value for BLACK is 0.

        // + means r/w
        // z means Z-pointer register pair
        let mut cursor = self.storage.as_ptr();
        unsafe { asm!("
            cpse %0, __zero_reg__ ; if value is zero, skip assigning to 0xff
            ldi %0, 0xFF
            clr __tmp_reg__ ; counter = 0
            1:
            ; (4x) push zero into screen buffer,
            ; then increment buffer position
            st Z+, %0
            st Z+, %0
            st Z+, %0
            st Z+, %0
            inc __tmp_reg__ ; increase counter
            brne 1b
            ; repeat for 256 loops
            ; (until counter rolls over back to 0)"
            : "+d" (value as u8),
              "+z" (cursor)
        ) }
    }

}
