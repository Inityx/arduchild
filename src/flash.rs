//! Facilities for storing constants in program memory

// = means write-only
// r means register allocated
// z means Z pointer register
#[inline(always)]
unsafe fn lpm(addr: *const u8) -> u8 {
    let value;
    asm!("lpm %0, Z" : "=r"(value) : "z"(addr));
    value
}

pub struct Flash(&'static [u8]);

impl Flash {
    pub unsafe fn from_progmem(data: *const u8, len: usize) -> Self {
        Self(core::slice::from_raw_parts(data, len))
    }

    pub fn get(&self, index: usize) -> u8 {
        unsafe { lpm(&self.0[index]) }
    }
}

impl<'a> IntoIterator for &'a Flash {
    type Item = u8;
    type IntoIter = Iter;
    fn into_iter(self) -> Self::IntoIter {
        Iter {
            start: self.0.as_ptr(),
            end: unsafe { self.0.as_ptr().add(self.0.len()) },
        }
    }
}

pub struct Iter {
    start: *const u8,
    end: *const u8,
}

impl Iterator for Iter {
    type Item = u8;
    fn next(&mut self) -> Option<Self::Item> {
        if self.start == self.end { return None; }

        let value = unsafe { lpm(self.start) };
        self.start = unsafe { self.start.offset(1) };
        Some(value)
    }
}

impl DoubleEndedIterator for Iter {
    fn next_back(&mut self) -> Option<Self::Item> {
        if self.start == self.end { return None; }

        self.end = unsafe { self.end.offset(-1) };
        Some(unsafe { lpm(self.end) })
    }
}
