//! # Arduchild
//!
//! `arduchild` is a hardware abstraction library
//! for the [Arduboy handheld console](https://arduboy.com).
//!
//! This library is losely based on the
//! [first-party Arduboy library](https://github.com/Arduboy/Arduboy),
//! and does not attempt to preserve its API.

#![no_std]
#![feature(asm, slice_patterns)]

pub mod rom;
pub mod screen;
pub mod buttons;
pub mod flash;
pub mod led;

struct Arduboy {
    screen: screen::Screen,
}


#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
